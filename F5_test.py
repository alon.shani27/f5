import pytest
import requests
from urllib.request import urlopen
from bs4 import BeautifulSoup

# Run first test
def test_1():
	checkStringInPage("Right-click in the box below to see one called 'the-internet'")

# Run second test
def test_2():	
	checkStringInPage("Alibaba")

# Function to check if string existing in the html page
def checkStringInPage(isStringExistingInPage):

	# Get url
	url = "https://the-internet.herokuapp.com/context_menu"

	# Read html page
	try:
		readHtml = urlopen(url).read()
	except AssertionError as msg:
		print(msg)

	# Parse html
	try:
		parseHtml = BeautifulSoup(readHtml, features="html.parser")
	except AssertionError as msg:
		print(msg)

	# get text 
	try:
		htmlContent = parseHtml.get_text()
	except AssertionError as msg:
		print(msg)

	# Check if string existing in the html page
	assert isStringExistingInPage in (htmlContent), "The string : " + isStringExistingInPage + " does not existing in the page"
